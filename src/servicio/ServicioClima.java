package servicio;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.URL;
import java.net.URLConnection;

import java.util.HashMap;
import java.util.Map;

import javax.xml.ws.Response;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class ServicioClima {
	private String API_KEY;
	private String ciudad;
	private String URL;


	public ServicioClima() {
		API_KEY = "9130fcb69e6cbc5ee9421d50f6027af4";
	}
	
	public void obtenerClima(String ciudad) {
		this.ciudad = ciudad;
		URL = "http://api.openweathermap.org/data/2.5/weather?q="+this.ciudad+"&appid="+API_KEY+"&lang=es&units=metric";
	}

	private Map<String,Object> obtenerClimaPrincipal() {
			Map<String,Object> infoClima = jsonToMap(traerDatos());
			return jsonToMap(infoClima.get("main").toString());		
	}
	
	public double obtenerHumedad() { 
		return (double) obtenerClimaPrincipal().get("humidity");		
	}
	
	public double obtenerSensacionTermica(){
		return (double) obtenerClimaPrincipal().get("feels_like");
	}
	
	public double obtenerTemperatura() {
		return (double) obtenerClimaPrincipal().get("temp");
	}
	
	public double obtenerTempMax() {
		return (double) obtenerClimaPrincipal().get("temp_max");
	}
	
	public double obtenerTempMin() {
		return (double) obtenerClimaPrincipal().get("temp_min");
	}
	
	public double obtenerPresionAtmosferica() {
		return (double) obtenerClimaPrincipal().get("pressure");
	}
	
	public double obtenerVelViento() { //Revisar Nombre
		Map<String,Object> infoClima = jsonToMap(traerDatos());
		return (double) jsonToMap(infoClima.get("wind").toString()).get("speed");		
}
	
	private static Map<String,Object> jsonToMap(String str) {
		Map<String,Object> map = new Gson().fromJson(str, new TypeToken<HashMap<String,Object>>(){}.getType());
		return map;
	}
	
	
	public String obtenerDescripcionClima() {
		Object obj = new JsonParser().parse(traerDatos());;
		JsonObject jsonObj = (JsonObject) obj;
		JsonArray gsonArr = (JsonArray) jsonObj.get("weather");
		JsonObject gsonObj = gsonArr.get(0).getAsJsonObject();
		String descripcion = gsonObj.get("description").getAsString();
  
		return descripcion;
	} 
	
	public String obtenerPais() {
		Map<String,Object> info = jsonToMap(traerDatos());
		return (String) jsonToMap(info.get("sys").toString()).get("country");
		
	}
				 
	private String traerDatos() {
		try {
			StringBuilder resultado = new StringBuilder();
			URL url = new URL(this.URL);
			URLConnection conexion = url.openConnection();
			BufferedReader rd = new BufferedReader(new InputStreamReader(conexion.getInputStream()));		
			String linea;
			
			while ( (linea = rd.readLine() ) != null) 
				resultado.append(linea);

			rd.close();
			return resultado.toString();
		}
		catch(Exception e) {
			return e.getMessage();
		}
	}

	
	
	
}