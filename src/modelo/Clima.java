package modelo;

public class Clima {
    private String descripcion;
    private double humedad;
    private double presionAtmosferica;
    private Temperatura temperatura;
    private double velocidadPromedioViento;
    
    
	public Clima(String descripcion, double humedad, double presionAtmosferica,Temperatura temperatura, double velocidadPromedioViento ) {
		
		this.descripcion = descripcion;
		this.humedad = humedad;
		this.presionAtmosferica = presionAtmosferica;
		this.temperatura = temperatura;
		this.velocidadPromedioViento = velocidadPromedioViento;
	}
	
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	public double getHumedad() {
		return humedad;
	}
	
	public void setHumedad(double humedad) {
		this.humedad = humedad;
	}
	
	public double getPresionAtmosferica() {
		return presionAtmosferica;
	}
	
	public void setPresionAtmosferica(double presionAtmosferica) {
		this.presionAtmosferica = presionAtmosferica;
	}


	public Temperatura getTemperatura() {
		return temperatura;
	}


	public void setTemperatura(Temperatura temperatura) {
		this.temperatura = temperatura;
	}


	public double getVelocidadPromedioViento() {
		return velocidadPromedioViento;
	}


	public void setVelocidadPromedioViento(double velocidadPromedioViento) {
		this.velocidadPromedioViento = velocidadPromedioViento;
	}
	
 
	
   
}