package modelo;

import modelo.Clima;

public class Ciudad {
	private String pais;
	private Clima clima;
	private String nombre;
	
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Clima getClima() {
		return clima;
	}
	
	public void setClima(Clima clima) {
		this.clima = clima;
	}
	
	public String getPais() {
		return pais;
	}
	
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	
	
}