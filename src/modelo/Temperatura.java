package modelo;

public class Temperatura {
	private double actual;
	private double max;
	private double min;
	private double sensacionTermica;
	

	public Temperatura(double actual, double max, double min, double sensacionTermica) {
		this.max = max;
		this.min = min;
		this.actual = actual;
		this.sensacionTermica = sensacionTermica;
	}

	public double getMax() {
		return max;
	}
	
	public void setMax(double max) {
		this.max = max;
	}
	
	public double getMin() {
		return min;
	}
	
	public void setMin(double min) {
		this.min = min;
	}

	public double getSensacionTermica() {
		return sensacionTermica;
	}

	public void setSensacionTermica(double sensacionTermica) {
		this.sensacionTermica = sensacionTermica;
	}

	public double getActual() {
		return actual;
	}

	public void setActual(double actual) {
		this.actual = actual;
	}
	
	
	

}
