package main;

import controlador.Controlador;
import modelo.Ciudad;
import vista.MenuFrame;

public class Main {
	@SuppressWarnings("unused")
	public static void main(String[] args){
		Ciudad modelo = new Ciudad();
		MenuFrame vista = new MenuFrame();
		Controlador controlador = new Controlador(vista,modelo);
	}
}