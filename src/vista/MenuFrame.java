package vista;


import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;

import java.awt.Color;
import javax.swing.JTextPane;

public class MenuFrame {

	private JFrame frame;
	private JLabel lblTempActual;
	private JLabel lblFondo;
	private JButton btnIniciar;
	private JLabel lblPais;
	private JLabel lblHumedad;
	private JLabel lblMin;
	private JLabel lblMax;
	private JLabel lblPresion;
	private JLabel lblSensacionTermica;
	private JLabel lblDescripcion;
	private JLabel lblVelViento;
	private JTextPane TextPane;

	public MenuFrame() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFocusable(true);
		
		frame.getContentPane().setFont(new Font("Louis George Caf�", Font.BOLD, 16));
		frame.setBounds(100, 50, 500, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);	
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		crearCarteles();
		
		crearTextPane();
		
		crearFondo();
		
		frame.setVisible(true);

	}

	private void crearCarteles() {
		btnIniciar = new JButton("Iniciar");
		btnIniciar.setVerifyInputWhenFocusTarget(false);
		btnIniciar.setBounds(357, 334, 89, 26);
		btnIniciar.setBackground(new Color(51, 102, 255));
		frame.getContentPane().add(btnIniciar);
				
		lblTempActual = new JLabel("");
		lblTempActual.setBackground(new Color(176, 224, 230));
		lblTempActual.setForeground(Color.WHITE);
		lblTempActual.setFont(new Font("Rockwell Extra Bold", Font.PLAIN, 30));
		lblTempActual.setHorizontalAlignment(SwingConstants.CENTER);
		lblTempActual.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Temperatura", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		lblTempActual.setBounds(34, 147, 171, 154);
		frame.getContentPane().add(lblTempActual);
		
		
		lblPais = new JLabel("");
		lblPais.setForeground(Color.WHITE);
		lblPais.setHorizontalAlignment(SwingConstants.CENTER);
		lblPais.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Pais", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(255, 255, 255)));
		lblPais.setBounds(34, 12, 171, 55);
		lblPais.setBackground(new Color(51, 102, 255));
		frame.getContentPane().add(lblPais);
		
		lblHumedad = new JLabel("");
		lblHumedad.setForeground(Color.WHITE);
		lblHumedad.setHorizontalAlignment(SwingConstants.CENTER);
		lblHumedad.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Humedad", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		lblHumedad.setBounds(260, 12, 185, 31);
		frame.getContentPane().add(lblHumedad);
		
		lblMin = new JLabel("");
		lblMin.setForeground(Color.WHITE);
		lblMin.setHorizontalAlignment(SwingConstants.CENTER);
		lblMin.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Temperatura Minima", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		lblMin.setBounds(261, 54, 185, 31);
		frame.getContentPane().add(lblMin);
		
		lblMax = new JLabel("");
		lblMax.setForeground(Color.WHITE);
		lblMax.setHorizontalAlignment(SwingConstants.CENTER);
		lblMax.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Temperatura Maxima", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		lblMax.setBounds(261, 90, 185, 31);
		frame.getContentPane().add(lblMax);
		
		lblPresion = new JLabel("");
		lblPresion.setForeground(Color.WHITE);
		lblPresion.setHorizontalAlignment(SwingConstants.CENTER);
		lblPresion.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Presion", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		lblPresion.setBounds(260, 132, 185, 31);
		frame.getContentPane().add(lblPresion);
		
		lblSensacionTermica = new JLabel("");
		lblSensacionTermica.setForeground(Color.WHITE);
		lblSensacionTermica.setHorizontalAlignment(SwingConstants.CENTER);
		lblSensacionTermica.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Sensacion Termica", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		lblSensacionTermica.setBounds(260, 215, 185, 31);
		frame.getContentPane().add(lblSensacionTermica);
		
		lblDescripcion = new JLabel("");
		lblDescripcion.setForeground(Color.WHITE);
		lblDescripcion.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Descripcion Del Clima", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		lblDescripcion.setHorizontalAlignment(SwingConstants.CENTER);
		lblDescripcion.setBounds(34, 78, 171, 55);
		frame.getContentPane().add(lblDescripcion);
		
		lblVelViento = new JLabel("");
		lblVelViento.setForeground(Color.WHITE);
		lblVelViento.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Velocidad Del viento", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		lblVelViento.setHorizontalAlignment(SwingConstants.CENTER);
		lblVelViento.setBounds(260, 174, 185, 31);
		frame.getContentPane().add(lblVelViento);
	}

	private void crearTextPane() {
		TextPane = new JTextPane();
		TextPane.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 18));
		TextPane.setForeground(Color.WHITE);
		TextPane.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		TextPane.setBounds(34, 334, 254, 26);
		TextPane.setBackground(new Color(51, 102, 255));
		frame.getContentPane().add(TextPane);
	}

	private void crearFondo() {
		lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon("Resources/fondo.jpg"));
		lblFondo.setBackground(Color.LIGHT_GRAY);
		lblFondo.setFont(new Font("Tahoma", Font.PLAIN, 27));
		lblFondo.setBounds(0, 0, 494, 371);
		frame.getContentPane().add(lblFondo);
	}
	
	public void alerta() {
		JOptionPane.showMessageDialog(frame, "No se encontro la region!");
		
	}

	public JFrame getFrame() {
		return frame;
	}

	public JLabel getLblTempActual() {
		return lblTempActual;
	}

	public JLabel getLblSensacionTermica() {
		return lblSensacionTermica;
	}

	public JLabel getLblGetPais() {
		return lblPais;
	}

	public JButton getBtnIniciar() {
		return btnIniciar;
	}

	public JLabel getLblHumedad() {
		return lblHumedad;
	}

	public JLabel getLblMin() {
		return lblMin;
	}

	public JLabel getLblMax() {
		return lblMax;
	}

	public JLabel getLblPresion() {
		return lblPresion;
	}

	public JLabel getLblDescripcion() {
		return lblDescripcion;
	}

	public JLabel getLblVelViento() {
		return lblVelViento;
	}

	public JTextPane getTextPane() {
		return TextPane;
	}

	
}