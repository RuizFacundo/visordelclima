package controlador;

import java.awt.event.ActionEvent;

import modelo.Ciudad;
import modelo.Clima;
import modelo.Temperatura;
import servicio.ServicioClima;
import vista.MenuFrame;

public class Controlador {
	private Ciudad ciudad;
	private MenuFrame vista;
	private ServicioClima service;
	
	public Controlador(MenuFrame vista, Ciudad modelo) {
		service = new ServicioClima();
		this.vista = vista;
		this.ciudad = modelo;
		this.vista.getBtnIniciar().addActionListener(p->Iniciar(p));
	}

	public void Iniciar(ActionEvent p) {	
		buscarClima();
		ActualizarVista();
		
	}


	private void ActualizarVista() {
		vista.getLblTempActual().setText(String.valueOf(ciudad.getClima().getTemperatura().getActual()+"�"));
		vista.getLblHumedad().setText(String.valueOf(ciudad.getClima().getHumedad())+"%");
		vista.getLblMax().setText(String.valueOf(ciudad.getClima().getTemperatura().getMax())+"�");
		vista.getLblMin().setText(String.valueOf(ciudad.getClima().getTemperatura().getMin())+"�");
		vista.getLblSensacionTermica().setText(String.valueOf(ciudad.getClima().getTemperatura().getSensacionTermica())+"�");
		vista.getLblPresion().setText(String.valueOf(ciudad.getClima().getPresionAtmosferica())+" hPa");
		vista.getLblDescripcion().setText(ciudad.getClima().getDescripcion());
		vista.getLblVelViento().setText(String.valueOf(ciudad.getClima().getVelocidadPromedioViento())+" KM/H");
		vista.getLblGetPais().setText(ciudad.getPais());
	}

	private void  buscarClima() {
		service.obtenerClima(vista.getTextPane().getText());
		this.ciudad.setPais(service.obtenerPais());
		String descripcion = service.obtenerDescripcionClima();
		double sensacionTermica = service.obtenerSensacionTermica();
		double humedad = service.obtenerHumedad(); //En porcentajes
		double presionAtmosferica = service.obtenerPresionAtmosferica();
		double tempMaxima = service.obtenerTempMax(); // En Celcius
		double tempMinima = service.obtenerTempMin(); // En Celcius
		double tempActual = service.obtenerTemperatura(); // En Celcius
		double VelViento = service.obtenerVelViento();	// En Km
		
		this.ciudad.setNombre(vista.getTextPane().getText());
		Temperatura temp = new Temperatura(tempActual, tempMaxima , tempMinima , sensacionTermica);
		ciudad.setClima(new Clima(descripcion , humedad , presionAtmosferica , temp, VelViento));
		
	}
}
